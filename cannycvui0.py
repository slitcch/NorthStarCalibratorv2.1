#!/usr/bin/python3
import cv2, cvui
import numpy as np
import pyrealsense2 as rs

"""
Returns R, T transform from src to dst
"""
def get_extrinsics(src, dst):
	extrinsics = src.get_extrinsics_to(dst)
	R = np.reshape(extrinsics.rotation, [3,3]).T
	T = np.array(extrinsics.translation)
	return (R, T)

"""
Returns a camera matrix K from librealsense intrinsics
"""
def camera_matriY(intrinsics):
	return np.array([[intrinsics.fx,             0, intrinsics.ppx],
					 [            0, intrinsics.fy, intrinsics.ppy],
					 [            0,             0,              1]])

"""
Returns the fisheye distortion from librealsense intrinsics
"""
def fisheye_distortion(intrinsics):
	return np.array(intrinsics.coeffs[:4])


dark = np.zeros((1600,2880),dtype=np.uint8)*255
bright = np.ones((1600,2880),dtype=np.uint8)*255

# Declare RealSense pipeline, encapsulating the actual device and sensors
pipe = rs.pipeline()
# allowed options: [option.exposure, option.gain, option.enable_auto_exposure, option.frames_queue_size, option.asic_temperature, option.motion_module_temperature, option.enable_mapping, option.enable_relocalization, option.enable_pose_jumping, option.enable_dynamic_calibration, option.enable_map_preservation]
cfg = rs.config()
prof = cfg.resolve(pipe)
s = prof.get_device().query_sensors()[0]
s.set_option(rs.option.enable_auto_exposure, 0)
s.set_option(rs.option.exposure, 30000)

s.set_option(rs.option.gain,6)

# Start streaming with our callback
pipe.start(cfg)

try:
	# Set up an OpenCV window to visualize the results
	WINDOW_TITLE = 'Realsense'
	CANNY_WINDOW_TITLE = 'canny'
	NS_WINDOW_TITLE = 'ns'
	IDK_WINDOW_TITLE = 'idk'
	CVUI_WINDOW_TITLE='cvui'


	cv2.namedWindow(IDK_WINDOW_TITLE,cv2.WINDOW_NORMAL)
	cv2.namedWindow(CVUI_WINDOW_TITLE,cv2.WINDOW_NORMAL)
	cv2.namedWindow(WINDOW_TITLE, cv2.WINDOW_NORMAL)
	cv2.namedWindow(CANNY_WINDOW_TITLE, cv2.WINDOW_NORMAL)
	cv2.namedWindow      (NS_WINDOW_TITLE, 0)#cv2.WINDOW_NORMAL)
	cv2.moveWindow       (NS_WINDOW_TITLE, 5100, 0)
	#cv2.waitKey(0)
	cv2.imshow(NS_WINDOW_TITLE,dark)
	cv2.setWindowProperty(NS_WINDOW_TITLE, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
	#cv2.namedWindow(HARRIS_WINDOW_TITLE, cv2.WINDOW_NORMAL)

	# Configure the OpenCV stereo algorithm. See
	# https://docs.opencv.org/3.4/d2/d85/classcv_1_1StereoSGBM.html for a
	# description of the parameters


	# Retreive the stream and intrinsic properties for both cameras
	profiles = pipe.get_active_profile()
	streams = {"left"  : profiles.get_stream(rs.stream.fisheye, 1).as_video_stream_profile(),
			   "right" : profiles.get_stream(rs.stream.fisheye, 2).as_video_stream_profile()}
	intrinsics = {"left"  : streams["left"].get_intrinsics(),
				  "right" : streams["right"].get_intrinsics()}

	# Print information about both cameras
	print("Left camera:",  intrinsics["left"])
	print("Right camera:", intrinsics["right"])

	# Translate the intrinsics from librealsense into OpenCV
	K_left  = camera_matriY(intrinsics["left"])
	D_left  = fisheye_distortion(intrinsics["left"])
	K_right = camera_matriY(intrinsics["right"])
	D_right = fisheye_distortion(intrinsics["right"])
	(width, height) = (intrinsics["left"].width, intrinsics["left"].height)



	showOnNS = dark.copy()
	iY = 0
	iX = 0
	print("alive")
	numSteps = 16
	fullyDarkImage = None # initialize because Python be stupid
	fullyWhiteImage = None
	mask = None
	copyWhiteStage = 1000000 # make it unreasonably far away
	stage = 0


	realsenseExposure = [21000.]
	realsenseGain = [3.]
	frame = np.zeros((650, 450, 3), np.uint8)

	# Size of trackbars
	width = 400

	# Init cvui and tell it to use a value of 20 for cv2.waitKey()
	# because we want to enable keyboard shortcut for
	# all components, e.g. button with label '&Quit'.
	# If cvui has a value for waitKey, it will call
	# waitKey() automatically for us within cvui.update().
	cvui.init(CVUI_WINDOW_TITLE, 20)

	while True:
		# Fill the frame with a nice color
		frame[:] = (49, 52, 49)

		cvui.beginColumn(frame, 20, 20, -1, -1, 6)
		
		cvui.text('realsense exposure')
		cvui.trackbar(width,realsenseExposure,200.,50000.)
		cvui.space(10)
		cvui.text('realsense gain')
		cvui.trackbar(width,realsenseGain,-1.,6.)
		cvui.space(10)
		
		#cvui.text

		# Exit the application if the quit button was pressed.
		# It can be pressed because of a mouse click or because 
		# the user pressed the 'q' key on the keyboard, which is
		# marked as a shortcut in the button label ('&Quit').
		if cvui.button('&Quit'):
			break

		
		cvui.endColumn()

		# Since cvui.init() received a param regarding waitKey,
		# there is no need to call cv.waitKey() anymore. cvui.update()
		# will do it automatically.
		cvui.update()

		cv2.imshow(CVUI_WINDOW_TITLE, frame)
		
		
		################################################################



		# Check if the camera has acquired any frames
		frames = pipe.wait_for_frames()
		f1 = frames.get_fisheye_frame(1).as_video_frame()
		f2 = frames.get_fisheye_frame(2).as_video_frame()
		left_data = np.asanyarray(f1.get_data())
		right_data = np.asanyarray(f2.get_data())

		image_left = cv2.cvtColor(left_data, cv2.COLOR_GRAY2RGB)
		image_right = cv2.cvtColor(right_data, cv2.COLOR_GRAY2RGB)
		both = np.hstack((image_left,image_right))
		whatis = cv2.cvtColor(image_left,cv2.COLOR_RGB2GRAY)
		if stage == 0:
			fullyDarkImage = both.copy()
		# used to be 100,200
		# used to be 150,300
		# 200,400 was too much
		canny = cv2.Canny(both,180,350)
		if type(mask) != type(None):
			canny -= (255-mask)
		#canny += harris
		cv2.imshow(WINDOW_TITLE, both)
		cv2.imshow(CANNY_WINDOW_TITLE,canny)
		cv2.imshow(NS_WINDOW_TITLE,showOnNS)

		key = cv2.waitKey(1)

		
		if key == ord('q'):
			break
		if key == ord('r'):
			showOnNS = bright.copy()
			if type(mask) == type(None):
				copyWhiteStage = stage+10 
			iY = 0
			iX = 0
		if key == ord('m'):
			print("doing mask!")
			fullyWhiteImage = both.copy()
			#mask = cv2.threshold(cv2.subtract(fullyDarkImage,fullyWhiteImage), thresh=53, maxval=1, type=cv2.THRESH_BINARY)[1]
			#mask = cv2.threshold(cv2.subtract(fullyWhiteImage,fullyDarkImage), thresh=53, maxval=1, type=cv2.THRESH_BINARY)[1]
			mask = cv2.subtract(fullyWhiteImage,fullyDarkImage*5)
			#mask = np.clip(mask[..., 1] * mask[..., 2], 0, 255)
			#mask = createMask(mask)*255
			# thresh used to be 5, which was too low for old high exposure
			# thresh used to be 50, which was too low old high exposure
			mask = cv2.threshold(mask, thresh=5, maxval=255, type=cv2.THRESH_BINARY)[1][:,:,1]

			cv2.imshow(IDK_WINDOW_TITLE,mask)            


			kernel = np.ones((5,5),np.uint8)
			
			mask = cv2.erode(mask,kernel,iterations=1)

			print(mask.shape)
			cv2.imshow(IDK_WINDOW_TITLE,mask)

		if key == ord('y'):
			# move on Y axis
			showOnNS[:int(iY*(1600/16))] = dark[:int(iY*(1600/16))]
			iY+=1
		if key == ord('x'):
			# move on X axis
			showOnNS[:,:int(iX*(1440/16))] = dark[:,:int(iX*(1440/16))]
			showOnNS[:,1440:1440+int(iX*(1440/16))] = dark[:,:int(iX*(1440/16))]
			iX+=1
		if key == ord('1'):
			cv2.imshow(IDK_WINDOW_TITLE,fullyWhiteImage)
		if key == ord("2"):
			cv2.imshow(IDK_WINDOW_TITLE,fullyDarkImage)
		if key == ord('g'):
			kernel = np.ones((5,5),np.uint8)
			
			mask = cv2.erode(mask,kernel,iterations=1)
			cv2.imshow(IDK_WINDOW_TITLE,mask)
		s.set_option(rs.option.exposure,realsenseExposure[0])
		s.set_option(rs.option.gain,realsenseGain[0])

			  

		stage += 1
			

		
finally:
	pipe.stop()